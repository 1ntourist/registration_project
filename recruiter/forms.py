from django import forms
from django.utils.datetime_safe import datetime

from .models import Recruiter


class RecruiterCreateForm(forms.ModelForm):
    last_name = forms.CharField(label='Фамилия')
    first_name = forms.CharField(label='Имя')
    middle_name = forms.CharField(label='Отчество')
    passport_id = forms.CharField(label='ID паспорта', max_length=9)
    slug = forms.IntegerField(label='Персональный номер', widget=forms.TextInput(attrs={
        'maxlength': '14'
    }), error_messages={'invalid': 'Введен неправильный номер'})
    area = forms.CharField(max_length=50)
    region = forms.CharField(label='Район')
    village = forms.CharField(label='Село', required=False)
    address = forms.CharField(label='Адрес')
    birthday = forms.DateField(label='Дата рождения', initial=datetime.now(), help_text='Формат даты, ДД/ММ/ГГГГ')

    class Meta:
        model = Recruiter
        exclude = ('created',)
        fields = '__all__'
