from django.urls import path
from .views import *

app_name = 'recruiter'

urlpatterns = [
    path('create/', RecruiterCreateView.as_view(), name='create'),
    path('list/', RecruiterListView.as_view(), name='list'),
    path('detail/<slug:slug>/', RecruiterDetailView.as_view(), name='detail'),
    path('update/<slug:slug>/', RecruiterUpdateView.as_view(), name='update'),
    path('delete/<slug:slug>/', RecruiterDeleteView.as_view(), name='delete'),
]
