from django.contrib import admin
from .models import Referral


@admin.register(Referral)
class RecruiterAdmin(admin.ModelAdmin):
    list_display = ['last_name', 'first_name', 'middle_name', 'passport_id']
    list_filter = ['area', 'region', 'village', 'status']
    raw_id_fields = ('recruiter', )