from django import forms
from django.utils.datetime_safe import datetime

from .models import Referral, STATUS_CHOICES


class ReferralCreateForm(forms.ModelForm):
    last_name = forms.CharField(label='Фамилия')
    first_name = forms.CharField(label='Имя')
    middle_name = forms.CharField(label='Отчество')
    passport_id = forms.CharField(label='ID паспорта', max_length=9)
    slug = forms.IntegerField(label='Персональный номер', widget=forms.TextInput(attrs={
        'maxlength': '14'
    }), error_messages={'invalid': 'Введен неправильный номер'})
    area = forms.CharField(label='Город, область')
    region = forms.CharField(label='Район')
    village = forms.CharField(label='Село', required=False)
    address = forms.CharField(label='Адрес')
    recruiter = forms.Select()
    birthday = forms.DateField(label='Дата рождения', initial=datetime.now(), help_text='Формат даты, ДД/ММ/ГГГГ')
    status = forms.ChoiceField(choices=STATUS_CHOICES, label='Статус')

    class Meta:
        model = Referral
        fields = '__all__'
        exclude = ('created', )
