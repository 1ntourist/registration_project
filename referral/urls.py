from django.urls import path
from .views import *

app_name = 'referral'

urlpatterns = [
    path('create/', ReferralCreateView.as_view(), name='create'),
    path('list/', ReferralListView.as_view(), name='list'),
    path('detail/<slug:slug>/', ReferralDetailView.as_view(), name='detail'),
    path('update/<slug:slug>/', ReferralUpdateView.as_view(), name='update'),
    path('delete/<slug:slug>/', ReferralDeleteView.as_view(), name='delete'),
    path('filter/<int:pk>', ReferralListView.referral_filter, name="filter"),

    # path('area/', ReferralArea.as_view(), name='area')
]