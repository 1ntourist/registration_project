from django.shortcuts import redirect, render
from django.core.paginator import Paginator
from django.views.generic import CreateView, TemplateView, ListView, DetailView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import (LoginRequiredMixin)
from .models import Referral
from .forms import ReferralCreateForm


class ReferralListView(LoginRequiredMixin, ListView):
    model = Referral
    template_name = 'list_of_referrals.html'
    paginate_by = 3
    context_object_name = 'referrals'
    ordering = ('last_name', 'first_name', 'middle_name', )

    def referral_filter(request, pk):
        referral = Referral.objects.all()
        paginator = Paginator(referral, 3)
        if pk == 1:
            referral = referral.order_by('last_name', 'first_name', 'middle_name', )
        elif pk == 2:
            referral = referral.order_by('-created', )
        elif pk == 3:
            referral = referral.order_by('recruiter__last_name', 'recruiter__first_name', 'recruiter__middle_name')
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        return render(request, "list_of_referrals.html", {"referrals": referral, 'page_obj': page_obj})


class ReferralDetailView(LoginRequiredMixin, DetailView):
    model = Referral
    template_name = 'detail_of_referrals.html'


class ReferralCreateView(LoginRequiredMixin, CreateView):
    model = Referral
    form_class = ReferralCreateForm
    template_name = 'create_of_referrals.html'


class ReferralUpdateView(LoginRequiredMixin, UpdateView):
    model = Referral
    form_class = ReferralCreateForm
    template_name = 'update_of_referrals.html'


class ReferralDeleteView(LoginRequiredMixin, DeleteView):
    model = Referral
    template_name = 'delete_of_referrals.html'
    success_url = reverse_lazy('referral:list')


class ReferralArea(ListView):
    model = Referral
    template_name = 'regions/area.html'
    context_object_name = 'areas'


# def referral_filter(request, pk):
#     """ Фильтр статей по дате
#     """
#     referral = Referral.objects.all()
#     if pk == 1:
#         referral = referral.order_by('last_name', 'first_name', 'middle_name', )
#     elif pk == 2:
#         referral = referral.order_by('created', )
#
#     return render(request, "list_of_referrals.html", {"referrals": referral})
